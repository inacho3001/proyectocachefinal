/*
 *  Cache simulation project
 *  Class UCR IE-521
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <Victimcache.h>

#define KB 1024
#define ADDRSIZE 32
using namespace std;

int lru_obl_replacement_policy (int idx,
                                int tag,
                                int associativity,
                                bool loadstore,
                                entry* cache_block,
                                entry* cache_block_obl,
                                operation_result* operation_result_cache_block,
				                operation_result* operation_result_cache_obl,
                                bool debug=false)
{
    bool hit_obl = false;
    bool prefetch = false; 
    bool insert_next = true;

    /* Check if A is a hit, if it's a hit, check if obl tag = 1. If miss, insert A + 1 */
    for(int i = 0; i < associativity; i++){
        if((cache_block[i].tag == tag) && (cache_block[i].valid == 1)){
        insert_next = false;
        if(cache_block[i].obl_tag == 1){
            hit_obl = true;
            cache_block[i].obl_tag = 0;
        }
        if(debug){
        cout<<"El bloque A es un hit en cache_block"<<endl;
        }
        lru_replacement_policy(idx,
                                tag,
                                associativity,
                                loadstore,
                                cache_block,
                                operation_result_cache_block,
                                debug);
        break;
        }
    }    

    /* if it was a hit with obl_tag = 1 */
    if(hit_obl == true){
        if(debug){
            cout<<"Fue hit con obl_tag = 1"<<endl << endl<< endl;
        }

        /* Check if A + 1 is on the cache already */  
        for(int i = 0; i < associativity; i++){
            if(cache_block_obl[i].tag == tag){
                prefetch = true;
                if(debug){
                    cout<<"A + 1 ya se encuentra en el cache, no se hace prefetch"<<endl;
                }
                break;
            }
        }  

        /* Insert block A as a hit */
        lru_replacement_policy(idx,
                                tag,
                                associativity,
                                loadstore,
                                cache_block,
                                operation_result_cache_block,
                                debug);

        /* A + 1 is not in the cache yet, insert it but keep values empty */ 
        if(prefetch == false){
            if(debug){
                cout<<"A + 1 no estaba en el cache, se inserta"<<endl;
            }    

            /* Insert block as a LRU miss but keep dirty bit untouched*/
            for(int i = 0; i < associativity; i++){
                if(cache_block_obl[i].rp_value == LRU){
                    if(cache_block_obl[i].dirty == true){
                        operation_result_cache_obl->dirty_eviction = true;

                    }
                    else{
                        operation_result_cache_obl->dirty_eviction = false;
                    }
                    operation_result_cache_obl->evicted_address = cache_block_obl[i].tag;\

                    /* if LOAD */
                    if(loadstore == LOAD){
                        cache_block_obl[i].dirty = false;
                        operation_result_cache_obl->miss_hit = MISS_LOAD;
                    }

                    /* if STORE */
                    else{
                        operation_result_cache_obl->miss_hit = MISS_STORE;
                    }

                    /* re order array */
                    for(int j = 0; j < associativity; j++){
                        if(cache_block_obl[j].rp_value > cache_block_obl[i].rp_value){
                            cache_block_obl[j].rp_value--;
                        }
                    }

                    /* Set missed block as MRU */
                    cache_block_obl[i].rp_value = associativity - 1; 

                    /* Update missed block entry */
                    cache_block_obl[i].obl_tag = 1;
                    cache_block_obl[i].tag = tag;
                    cache_block_obl[i].valid = true;
                    break;
                    
                }
            }
        } 

    }
    


    /* Block A is not in the cache */
    if(insert_next == true){
        if(debug){
            cout<<"El bloque A no esta en el cache, se inserta"<<endl;
        }

        /* Insert block A as a miss and check if block A + 1 is a miss */
        lru_replacement_policy(idx,
                                tag,
                                associativity,
                                loadstore,
                                cache_block,
                                operation_result_cache_block,
                                debug);

        /* Check if A + 1 is on the cache already */  
        for(int i = 0; i < associativity; i++){
            if(cache_block_obl[i].tag == tag){
                prefetch = true;
                if(debug){
                    cout<<"A + 1 ya se encuentra en el cache, no se hace prefetch"<<endl;
                }
                break;
            }
        } 

        /* A + 1 is not in the cache, insert it but keep values empty */
        if(prefetch == false){
            for(int i = 0; i < associativity; i++){
                if(cache_block_obl[i].rp_value == LRU){

                    /* Insert block as a LRU miss but keep dirty bit untouched*/
                    if(cache_block_obl[i].dirty == true){
                        operation_result_cache_obl->dirty_eviction = true;
                    }
                    else{
                        operation_result_cache_obl->dirty_eviction = false;
                    }
                    operation_result_cache_obl->evicted_address = cache_block_obl[i].tag;\

                    /* if LOAD */
                    if(loadstore == LOAD){
                        cache_block_obl[i].dirty = false;
                        operation_result_cache_obl->miss_hit = MISS_LOAD;
                    }

                    /* if STORE */
                    else{
                        operation_result_cache_obl->miss_hit = MISS_STORE;
                    }

                    /* re order array */
                    for(int j = 0; j < associativity; j++){
                        if(cache_block_obl[j].rp_value > cache_block_obl[i].rp_value){
                            cache_block_obl[j].rp_value--;
                        }
                    }

                    /* Set missed block as MRU */
                    cache_block_obl[i].rp_value = associativity - 1; 
                    
                    /* Update missed block entry */
                    cache_block_obl[i].obl_tag = 1;
                    cache_block_obl[i].tag = tag;
                    cache_block_obl[i].valid = true;
                    break;
                    
                }
            }
        }                     
    }
           

                                        
    return OK;
}
