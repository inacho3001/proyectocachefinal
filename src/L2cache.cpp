/*
 *  Cache simulation project
 *  Class UCR IE-521
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <L2cache.h>

#define KB 1024
#define ADDRSIZE 32
using namespace std;

int lru_replacement_policy_l1_l2(const entry_info *l1_l2_info,
				 bool loadstore,
				 entry* l1_cache_blocks,
				 entry* l2_cache_blocks,
				 operation_result* l1_result,
				 operation_result* l2_result,
				 bool debug) 
{


	bool hit_L1 = false;



	//check for hits in L1
	for (int i = 0; i < l1_l2_info->l1_assoc; i++)
	{	
		if (l1_cache_blocks[i].tag == l1_l2_info->l1_tag && hit_L1 == false)
		{
			hit_L1 = true;
		}
	}

	//In case of a hit in L1
	if (hit_L1 == true)
	{
		lru_replacement_policy (l1_l2_info->l1_idx,
                             l1_l2_info->l1_tag,
                             l1_l2_info->l1_assoc,
                             loadstore,
                             l1_cache_blocks,
                             l1_result,
                             debug);

         //if LOAD 

         if(loadstore == LOAD){
            l2_result->miss_hit = HIT_LOAD;
         }
         // if STORE 

         else{
            l2_result->miss_hit = HIT_STORE;
         }
	}
	else
	{

	// Eviction for L2
	lru_replacement_policy (l1_l2_info->l2_idx,
                             l1_l2_info->l2_tag,
                             l1_l2_info->l2_assoc,
                             loadstore,
                             l2_cache_blocks,
                             l2_result,
                             debug);
   
   	// Eviction for L1
	lru_replacement_policy (l1_l2_info->l1_idx,
                             l1_l2_info->l1_tag,
                             l1_l2_info->l1_assoc,
                             loadstore,
                             l1_cache_blocks,
                             l1_result,
                             debug);
	}
   
	return OK;
}
