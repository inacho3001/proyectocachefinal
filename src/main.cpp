#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <bits/stdc++.h>
#include <time.h>
#include <L1cache.h>
#include <debug_utilities.h>
#define KB 1024
#define ADDRSIZE 32

using namespace std;

  /* Print function */
void print_cache_results(int size, int associativity, int block_size, int rp, int ic_t, float load_h, float load_m, float store_h, float store_m, int dirty_evic)
{
  float total_m = load_m + store_m;
  float total_h = load_h + store_h;
  float missrate = (float)total_m/(float)(total_h + total_m);
  cout << "_______________________________________" << endl << "Cache parameters:"<< endl <<"_______________________________________\n" << endl;
  cout << "Cache Size (KB):               "<< size << endl;
  cout << "Cache Associativity:           "<< associativity << endl;
  cout << "Cache Block Size (bytes):      "<< block_size << endl;
  if(rp == LRU){
    cout << "Replacement Policy:            LRU" << endl;
  }
  else if(rp == NRU){
    cout << "Replacement Policy:            NRU" << endl;
  }
  else if(rp == RRIP){
    cout << "Replacement Policy:            SRRIP" << endl;
  }
  cout << "_______________________________________" << endl << "Simulation results:"<< endl <<"_______________________________________\n" << endl;
  cout << "CPU time (cycles):             " << (float)ic_t*(1 + missrate*20)/(float)(total_h+total_m) << endl;
  cout << "AMAT(cycles):                  "<< 1 +  (total_m*20)/(total_h + total_m) << endl;
  cout << "Overall miss rate::            "<< total_m/(total_h + total_m) << endl;
  cout << "Read miss rate:                "<< load_m/(total_h + total_m) << endl;
  cout << "Dirty evictions:               "<< dirty_evic << endl;
  cout << "Load misses:                   "<< load_m << endl;
  cout << "Store misses:                  "<< store_m << endl;
  cout << "Total misses:                  "<< total_m << endl;
  cout << "Load hits:                     "<< load_h << endl;
  cout << "Store hits:                    "<< store_h << endl;
  cout << "Total hits:                    "<< total_h << endl;
  cout << "_______________________________________" << endl;

  exit(0);
}


  /* MAIN */
int main(int argc, char * argv []) {
  
  /* Parse arguments */
  int size, asociativity, block_size, rp;

  for(size_t i = 1; i < argc; i++){ // reads stdin
    if(strcmp(argv[i], "-t") == 0){ // when argv[i] = keyword
      size = atoi(argv[i+1]); // store the parameter (located at argv[i+1]) for each keyword
    }
    else if(strcmp(argv[i], "-l") == 0){
      block_size = atoi(argv[i+1]);
    }
    else if(strcmp(argv[i], "-a") == 0){
      asociativity = atoi(argv[i+1]);
    }
    else if(strcmp(argv[i], "-rp") == 0){
      rp = atoi(argv[i+1]);
      if (rp == RANDOM)
      {
      srand(time(NULL)); 
      rp = rand()%3;
      }
    }
  }

  /* Get tag, index and offset length */
  struct cache_params cache_p;
  cache_p.size = size;
  cache_p.block_size = block_size;
  cache_p.asociativity = asociativity;
  struct cache_field_size field_s;
  field_size_get(cache_p, &field_s);

  /* Create cache */
  entry **get_cache = cache_blocks(asociativity, field_s.idx, rp);

  /* Create operation_result */
  struct operation_result res;

  /* Get trace's lines and start your simulation */
  int loadstore, idx, tag, ic;
  int ic_t = 0;
  int debug = 0;
  char lineP[200];
  char address_in[10];
  long address;
  float load_h = 0;
  float store_h = 0;
  float load_m = 0;
  float store_m = 0;
  int dirty_evic = 0;
  while(fgets(lineP, 200, stdin)!= NULL){
    if(lineP[0] == 35){                                                                                 /* if line is valid */
      sscanf(lineP, "%*s %d %s %d", &loadstore, address_in, &ic);
      ic_t = ic_t + ic;
      address = strtol(address_in, NULL, 16);
    }
    else{
      break;
    }
    address_tag_idx_get(address, field_s, &idx, &tag);
    switch (rp)
    {
    case LRU:
      lru_replacement_policy(idx, tag, asociativity, loadstore, get_cache[idx], &res, debug);
      break;
    case NRU:
      nru_replacement_policy(idx, tag, asociativity, loadstore, get_cache[idx], &res, debug);
      break;
    case RRIP:
      srrip_replacement_policy(idx, tag, asociativity, loadstore, get_cache[idx], &res, debug);
      break; 
    default:
      lru_replacement_policy(idx, tag, asociativity, loadstore, get_cache[idx], &res, debug);
      break;
    }

    /* Metrics */
    if (res.miss_hit == HIT_LOAD){
    load_h++;
    } 
    else if (res.miss_hit == MISS_LOAD){
      load_m++;
    }
    else if (res.miss_hit == HIT_STORE){
      store_h++;
    }
    else if (res.miss_hit == MISS_STORE){
      store_m++;
    }
    if (res.dirty_eviction == true){
      dirty_evic++;
    }
  }

  /* Print results */
  print_cache_results(size, asociativity, block_size, rp, ic_t, load_h, load_m, store_h, store_m, dirty_evic);
  
return 0;
}


