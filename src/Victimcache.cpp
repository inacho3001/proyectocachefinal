/*
 *  Cache simulation project
 *  Class UCR IE-521
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <Victimcache.h>

#define KB 1024
#define ADDRSIZE 32
using namespace std;

int lru_replacement_policy_l1_vc(const entry_info *l1_vc_info,
    	                      	 bool loadstore,
       	                    	 entry* l1_cache_blocks,
       	                  	 entry* vc_cache_blocks,
        	                 operation_result* l1_result,
              	              	 operation_result* vc_result,
                	         bool debug)
{

	bool hit_l1 = false;
	bool hit_vc = false;
	bool active_vc = false;

	/* Check if L1 is full to activate VC*/
	int count_valids_active = 0;
	for (size_t i = 0; i < l1_vc_info->l1_assoc; i++)
	{
		if (l1_cache_blocks[i].valid == true)
		{
			count_valids_active++;	
		}
	}
	if (count_valids_active == l1_vc_info->l1_assoc)
	{
		active_vc = true;
	}	
		

	/*Check hit in L1*/	
	for (int i = 0; i < l1_vc_info->l1_assoc; i++)
	{
		if (l1_vc_info->l1_tag == l1_cache_blocks[i].tag && hit_l1 == false)
		{
			hit_l1 = true;
		}
	}

	/*Check hit VC*/
	for (int i = 0; i < l1_vc_info->vc_assoc; i++)
	{
		if (l1_vc_info->l1_tag == vc_cache_blocks[i].tag && hit_vc == false)
		{
			hit_vc = true;
		}
	}

	/* In case of hit in L1*/
	if(hit_l1){
		lru_replacement_policy (l1_vc_info->l1_idx,
                             l1_vc_info->l1_tag,
                             l1_vc_info->l1_assoc,
                             loadstore,
                             l1_cache_blocks,
                             l1_result,
                             debug);
		
	}

	/* In case of not hit in L1 but hit in VC*/
	if(hit_l1 == false && hit_vc == true){
		lru_replacement_policy (l1_vc_info->l1_idx,
                             l1_vc_info->l1_tag,
                             l1_vc_info->l1_assoc,
                             loadstore,
                             l1_cache_blocks,
                             l1_result,
                             debug);

		/* Swap blocks between L1 and VC */
		for (int i = 0; i < l1_vc_info->vc_assoc; i++)
		{
			if (l1_vc_info->l1_tag == vc_cache_blocks[i].tag)
			{
				vc_cache_blocks[i].tag = l1_result->evicted_address;
			}
		}
		
		/* if LOAD */

        if(loadstore == LOAD){
        	vc_result->miss_hit = HIT_LOAD;
        }

        /* if STORE */
		else{
        	vc_result->miss_hit = HIT_STORE;
        }
	}

	/* In case of not hit in both caches*/
	if(hit_l1 == false && hit_vc == false){
		

		lru_replacement_policy (l1_vc_info->l1_idx,
                             l1_vc_info->l1_tag,
                             l1_vc_info->l1_assoc,
                             loadstore,
                             l1_cache_blocks,
                             l1_result,
                             debug);

		/* Set dirty eviction true if dirty bit is 1*/
		if(vc_cache_blocks[l1_vc_info->vc_assoc-1].dirty == true){
           vc_result->dirty_eviction = true;
        }
        else{
           vc_result->dirty_eviction = false;
        }

		/*Evicted address for VC*/
		vc_result->evicted_address = vc_cache_blocks[l1_vc_info->vc_assoc-1].tag;

        /* if LOAD */

        if(loadstore == LOAD){
           vc_cache_blocks[l1_vc_info->vc_assoc-1].dirty = false;
           vc_result->miss_hit = MISS_LOAD;
        }
        /* if STORE */

        else{
           vc_cache_blocks[l1_vc_info->vc_assoc-1].dirty = true;
           vc_result->miss_hit = MISS_STORE;
        }

		/* Replace blocks in VC if it is necessary*/
		if (active_vc == true)
		{
			/* FIFO policy */					
			int temp = vc_cache_blocks[0].tag;
			bool temp_valid = vc_cache_blocks[0].valid;
			bool temp_dirty = vc_cache_blocks[0].dirty;
			for (int i = 0; i < l1_vc_info->vc_assoc-1; i++)
			{	
				int aux = vc_cache_blocks[i+1].tag;
				bool aux_valid = vc_cache_blocks[i+1].valid;
				bool aux_dirty = vc_cache_blocks[i+1].dirty;
				vc_cache_blocks[i+1].tag = temp;
				vc_cache_blocks[i+1].valid = temp_valid;
				vc_cache_blocks[i+1].dirty = temp_dirty;
				temp = aux;
				temp_valid = aux_valid;
				temp_dirty = aux_dirty;
			}
			vc_cache_blocks[0].tag = temp;
			vc_cache_blocks[0].valid = temp_valid;
			vc_cache_blocks[0].dirty = temp_dirty;
			vc_cache_blocks[0].tag = l1_result->evicted_address;

			/*Updates valid and dirty bit in VC*/
			int count_valids = 0;
			for (size_t i = 0; i < l1_vc_info->l1_assoc; i++)
			{
				if (l1_cache_blocks[i].valid == true)
				{
					count_valids++;	
				}
			}
			if (count_valids == l1_vc_info->l1_assoc)
			{
				vc_cache_blocks[0].valid = true;
			}
			else
			{
				vc_cache_blocks[0].valid = false;
			}
			if (l1_result->dirty_eviction)
			{
				vc_cache_blocks[0].dirty = true;
			}
			else
			{
				vc_cache_blocks[0].dirty = false;
			}
			
			
		}
		
	}
   	
   	return OK;
}
